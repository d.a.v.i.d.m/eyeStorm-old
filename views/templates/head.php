<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= htmlspecialchars($title); ?></title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?= htmlspecialchars($description); ?>" />
	<meta name="google-site-verification" content="scP3iQq0RJSIb9oi5kqpf3JUyZrc8SMFVMPCo6dOJiM" />

    <link rel="shortcut icon" href="/images/tabc_logo.png" type="image/x-icon">
    <link rel="canonical" href="https://tabceots.com" />




  <!-- when offline-->
<!--  <script defer type="text/javascript" src="/scripts/jquery-3.1.1.min.js"></script>-->

       <!--
              <script defer
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
-->
	 <link type="text/css" rel="stylesheet" href="/styles/stormStyles.min.css?1.1" />


	  <script defer type="text/javascript" src="/scripts/stormScripts.js?1.1"></script>


</head>
<body>