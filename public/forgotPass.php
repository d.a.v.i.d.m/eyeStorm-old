<?php
session_start();

require_once("../controller/vendor/autoload.php");



$Page = new Page();

$Redirect = new Redirect();
$User = new User();
$token = $User->getJWT();

if (isset($token->id)) {
    $Redirect->toHome();
    exit;
}


$Page->render("forgotPasswd", "Recover Account", "Recover account in event of lost password");
exit;

?>