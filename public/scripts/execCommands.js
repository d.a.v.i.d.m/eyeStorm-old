var commands = [{

	"cmd": "bold",
}, {
	"cmd": "italic"
}, {
	"cmd": "underline"
}, {
	"cmd": "heading",
	"val": "1-6"
}, {
	"cmd": "fontSize",
	"val": "1-7"
}, {
	"cmd": "insertOrderedList"
}, {
	"cmd": "insertUnorderedList"
}, {
	"cmd": "justifyFull"
}, {
	"cmd": "justifyLeft"
}, {
	"cmd": "justifyRight"
}, {
	"cmd": "justifyCenter"
}, {
	"cmd": "strikeThrough"
}, {
	"cmd": "subscript"
}, {
	"cmd": "superscript"
}, {
	"cmd": "insertImage",
    "val": "https://dummyimage.com/160x90"
}, {
	"cmd": "createLink",
	"val": "www.example.com",
}, {
	"cmd": "unlink"
}, {
	"cmd": "hideFromPreview",
	"val": "Hide element from main page"
}
];