<?php



require_once(__DIR__."/../controller/vendor/autoload.php");

$Page = new Page();
$Val = new Validate();
$User = new User();


if (!$User->exists(...$Val->filterArgs([$_GET["user"]]))) {

    $Redirect = new Redirect();
    $Redirect->to404();
}

$Page->render("settingsTable", "User Profile", "Torah Academy Of Bergen County's Eye Of the Storm user profile");

?>