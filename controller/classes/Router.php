<?php

require_once(__DIR__."/../vendor/autoload.php");

class Router {

    public $url, $method, $safeInput, $input;


    /**
      * Sets instance variables
      *
      * $this->url = request uri
      * $this->method = request method
      * $this->safeInput = sanitize input
      * $this->input = unsanitized input (since for articles for example, must have actual html)
      */
    public function __construct() {


        $this->method = $_SERVER["REQUEST_METHOD"];

        $input = ($this->method == "GET") ? $_GET : $this->getRequestBodyData();

        if (!$input) {
            return false;
        }

        $Validate = new Validate();

        $this->url = $_REQUEST["field"];

        $this->safeInput = $Validate->filterArgs($input);
        $this->input = $input;


        $this->failFast();

        $result = $this->findFile();

        if (!is_string($result)) {
            echo json_encode($result);
        }
        else {
            echo $result;
        }
    }

    public function getRequestBodyData() {


        $request_body = file_get_contents('php://input');

        if ($this->url == "story") {

            $decoded = rawurldecode($request_body);

            return json_decode($decoded, true);
        }

        return json_decode($request_body, true);
    }


    public function findFile() {

        if (array_key_exists("password", $this->input)) {

            $this->safeInput["password"] = $this->input["password"]; // put unsanitized html with the other sanitized data

        }

        switch($this->url) {

            case "story":
                $Article = new Article();

                if ($this->method == "POST") {

                    $this->safeInput["txtArea"] = $this->input["txtArea"]; // put unsanitized html with the other sanitized data
                    list($this->safeInput, $this->input) = [$this->input, $this->safeInput]; // then put unsanitized data back into $this->input
                }
                else if ($this->method == "PUT") {

                    $this->safeInput["edit"] = $this->input["edit"]; // put unsanitized html with the other sanitized data
                    list($this->safeInput, $this->input) = [$this->input, $this->safeInput]; // then put unsanitized data back into $this->input
                }

                return $Article->route($this->method, $this->safeInput);

            case "articleGroup":
                $ArticleGroup = new ArticleGroup();
                return $ArticleGroup->route($this->method, $this->safeInput);

            case "comment":

                if ($this->method == "POST") {

                    $this->safeInput["content"] = $this->input["content"]; // put unsanitized html with the other sanitized data
                    list($this->safeInput, $this->input) = [$this->input, $this->safeInput]; // then put unsanitized data back into $this->input
                }

                $Comment = new Comment();
                return $Comment->route($this->method, $this->safeInput);

            case "userStatus":

                $User = new User();
                return $User->routeToken($this->method, $this->safeInput);

            case "userGroup":
                $UserGroup = new UserGroup();
                return $UserGroup->route($this->method, $this->safeInput);

            case "user":
                $User = new User();
                return $User->route($this->method, $this->safeInput);

            case "issue":
                $Issue = new Issue();
                return $Issue->route($this->method, $this->safeInput);

            case "previews":
                $Issue = new Issue();
                return $Issue->getPreviews($this->safeInput);

            case "mission":
                // not really any good place to put logic for editing misson, so tossing in here
                return $this->editMission();

        }
    }

    /**
      * Fail fast: If user isn't logged in, under no circumstances can they use any http nouns besides GET
      * Also checks if http method is implemented
      */
    public function failFast() {

        $User = new User();

        // can't change any data if not at the very least logged in, unless creating an account
        if (in_array($this->method, ["PUT", "POST", "DELETE"]) && !$User->isLoggedIn() &&
             ($this->url != "user" && $this->method != "POST") && $this->url != "userStatus") {

                Utilities::setHeader(401);
                exit;
        }

        if (!in_array($this->method, ["GET", "POST", "PUT", "DELETE"])) {

            Utilities::setHeader(404);
            exit;
        }

        // all arguments that must be there for actions to work
        $requiredKeys = [
            "story" => [

                "POST" => ["txtArea", "name", "type[]"],
                "PUT" => ["edit", "issue", "name"],
                "DELETE" => ["issue", "name", "password"],
                "GET" => ["issue", "name"]
            ],

            "articleGroup" => [
                "PUT" => ["artId[]"],
                "DELETE" => ["delArt[]", "password"],
            ],

            "comment" => [
                "POST" => ["issue", "url", "content"],
                "DELETE" => ["id"],
                "GET" => ["issue", "url"]
            ],

            "userGroup" => [
                "PUT" => ["lvl[]", "name[]", "pass"],
                "DELETE" => ["delAcc[]", "password"]
            ],

            "user" => [
                "POST" => ["username", "fullName", "password", "confirmation", "email", "lvl"],
                "DELETE" => ["delAcc"]
            ],

            "issue" => [
                "PUT" => ["issue", "password"]
            ],
        ];


        if (!empty($requiredKeys[$this->url][$this->method]) && array_diff($requiredKeys[$this->url][$this->method], array_keys($this->safeInput))) {
            Utilities::setHeader(422, "missing required field");
            exit;
        }
    }

    // not exactly sure where this should go
    private function editMission() {

        $Article = new Article();
        $User = new User();

        $token = $User->getJWT();

        if (isset($this->input["edit"])) {

            $User = new User();

            if (!$User->isLoggedIn() || $token->level < 3) {
                Utilities::setHeader(401);
                exit;
            }

            $filteredEdit = $Article->stripTags($this->input["edit"]);

            $status = file_put_contents("../../views/missionView.html", "<link type='text/css' rel='stylesheet' href='/styles/stormStory.min.css?1.1' />" .

                                                                    "<script defer src='/scripts/execCommands.min.js'></script>".
                                                                    "\n<script defer src='/scripts/mission.js'></script>".
                                                                    "\n<div class='container'>\n".

                                                                        "<h1>Mission</h1>\n".
                                                                        "<div id='missionEdit'>" .
                                                                            $filteredEdit .
                                                                        "</div>".
                                                                        "</div>");
            Utilities::setHeader(200, "mission edited");
        }
    }
}





?>