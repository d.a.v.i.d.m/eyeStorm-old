<?php

require_once(__DIR__."/../vendor/autoload.php");


class Info {


    /**
    * @param $username - valid username
    *
    * @return all article info published by $username
    */
    public function getArticlesFrom(string $username) {

        $User = new User();

        $db = new MyDB();

        if (!$User->exists($username)) {
            return false;
        }


       $artQuery = $db->catchMistakes("SELECT URL, CREATED, CONCAT(TAG1, IFNULL(CONCAT(', ', TAG2), ''), IFNULL(CONCAT(', ', TAG3), '')) AS TAGS, VIEWS,
                           PAGEINFO.ID AS ART_ID, ISSUE
                           FROM PAGEINFO
                            LEFT JOIN ISSUES
                            ON PAGEINFO.ISSUE = ISSUES.NUM
                            JOIN TAGS
                            ON TAGS.ART_ID = PAGEINFO.ID
                            WHERE AUTHORID = (SELECT ID FROM USERS WHERE USERNAME = ?) AND (ISPUBLIC = ? OR ?)", [$username, !$User->isLoggedIn(), $User->isLoggedIn()]);

        $artQuery->setFetchMode(PDO::FETCH_ASSOC);
        return $artQuery->fetchAll();
    }


    /**
      * @param $issue - valid issue number or tag name
      */
    function getLedesFromIssue(string $getFrom = "") {

        $isTag = is_numeric($getFrom) ? false : true;


        $db = new MyDB();
        $User = new User();

        if (!$getFrom || !$isTag || $getFrom == "all") {

            $Issue = new Issue();

            $maxIssue = (!!$User->isLoggedIn()) ? max($Issue->getMax(true), $Issue->getMax()) : $Issue->getMax(true);

            $getFrom = ($getFrom == "all") ? $maxIssue : $getFrom;

            $issueToGet = (empty($getFrom) || ($getFrom + 1) > $maxIssue) ? $maxIssue : $getFrom;

            $pageInfo = $db->catchMistakes("SELECT URL, LEDE, VIEWS, ISSUE
                                             FROM PAGEINFO
                                             WHERE ISSUE = ?
                                             ORDER BY DISPLAY_ORDER DESC", $issueToGet);
        }

        else {
            $pageInfo = $db->catchMistakes("SELECT URL, LEDE, VIEWS, ISSUE
                                               FROM PAGEINFO
                                               LEFT JOIN ISSUES
                                               ON NUM = ISSUE
                                               WHERE ID
                                                   IN (SELECT ART_ID FROM TAGS WHERE ? IN (TAG1, TAG2, TAG2))
                                               AND (ISPUBLIC = ? OR ?)
                                               ORDER BY ISSUE", [$getFrom, !$User->isLoggedIn(), $User->isLoggedIn()]);
        }

        $pageInfo->setFetchMode(PDO::FETCH_ASSOC);

        return $pageInfo->fetchAll();

    }


    /**
    * @param $issue - issue number
    *
    * @return info about articles for admins (/modifyArticles), array of associative arrays
    *  [0] is array of "URL", "CREATED", "AUTHOR_NAME", csv of "TAGS", "VIEWS", "DISPLAY_ORDER", "ART_ID", "AUTHOR_USERNAME"
    *  [1] is all tags ever used for articles
    *  [2] - "NAME", "ISPUBLIC", "NUM", "MAX"
    */
    public function getPageInfo(int $issue = null) {

        $User = new User();
        $db = new MyDB();
        $Issue = new Issue();
        $token = $User->getJWT();

        if (!$User->isLoggedIn() || $token->level < 3) {
            Utilities::setHeader(401);
            return false;
        }

        $max = max($Issue->getMax(true), $Issue->getMax());

        $getFrom = ($issue && is_numeric($issue) && $issue < $max) ? $issue : $max;

        $query = $db->catchMistakes("SELECT URL, CREATED, CONCAT(F_NAME, ' ', IFNULL(M_NAME, ''), ' ', L_NAME) AS AUTHOR_NAME,
                             CONCAT(TAG1, IFNULL(CONCAT(', ', TAG2), ''), IFNULL(CONCAT(', ', TAG3), '')) AS TAGS, VIEWS, DISPLAY_ORDER,
                             PAGEINFO.ID AS ART_ID, USERNAME AS AUTHOR_USERNAME
                             FROM PAGEINFO
                             LEFT JOIN USERS
                             ON USERS.ID = AUTHORID
                             LEFT JOIN TAGS
                             ON ART_ID = PAGEINFO.ID
                             WHERE ISSUE = ?", $getFrom);

        $query->setFetchMode(PDO::FETCH_ASSOC);

        $tagQuery = $db->catchMistakes("SELECT DISTINCT TAG1, TAG2, TAG3 FROM TAGS");

        $tagQuery->setFetchMode(PDO::FETCH_NUM);

        $issueQuery = $db->catchMistakes("SELECT NAME, ISPUBLIC, NUM FROM ISSUES WHERE NUM = ?", $getFrom);

        $issueQuery->setFetchMode(PDO::FETCH_ASSOC);

        $issueInfo = $issueQuery->fetchAll();

        if (!$issueInfo) {
            return false;
        }

        $maxObj["MAX"] = $max;

        $issueInfo[0] = array_merge($issueInfo[0], $maxObj);

        // merges the multi array of tags into 1 array, removes duplicate values, removes null,
        // then extracts the values (since the indices are messed up now)
        $uniqTags = array_values(array_filter(array_unique(array_merge(...$tagQuery->fetchAll()))));

        return array_merge([$query->fetchAll(), $uniqTags], $issueInfo);
    }

    /**
      * @return table form with info about all users in the form array[0] = current user's level, rest of indices objects
      */
    public function getUsersInfo() {

        $db = new MyDB();
        $User = new User();
        $token = $User->getJWT();

        if ($User->isLoggedIn()) {

            $queryInfo = $db->catchMistakes("SELECT
                CONCAT(USERS.L_NAME, ', ', USERS.F_NAME, ' ', IFNULL(USERS.M_NAME, '')) AS NAME, LEVEL,
                COUNT(PAGEINFO.ID) AS ARTICLES,
                IFNULL(SUM(PAGEINFO.VIEWS), 0) AS VIEWS, USERS.ID,
                TRIM(
                    LEADING '.' FROM TRIM(
                        TRAILING '@tabc.org' FROM EMAIL
                        )
                    ) AS PROFILE_LINK
                FROM USERS
                LEFT JOIN PAGEINFO
                ON USERS.ID = PAGEINFO.AUTHORID
                GROUP BY USERS.ID
                ORDER BY USERS.L_NAME DESC");
        }

        else {

            $queryInfo = $db->catchMistakes("SELECT
                            CONCAT(USERS.L_NAME, ', ', USERS.F_NAME, ' ', IFNULL(USERS.M_NAME, '')) AS NAME,
                            COUNT(PAGEINFO.ID) AS ARTICLES,
                            IFNULL(SUM(VIEWS), 0) AS VIEWS, USERS.ID,
                            TRIM(
                                LEADING '.' FROM TRIM(
                                    TRAILING '@tabc.org' FROM EMAIL
                                    )
                                ) AS PROFILE_LINK
                            FROM USERS
                            LEFT JOIN PAGEINFO
                            ON USERS.ID = PAGEINFO.AUTHORID AND PAGEINFO.ISSUE IN (SELECT NUM FROM ISSUES WHERE ISPUBLIC)
                            GROUP BY USERS.ID
                            ORDER BY USERS.L_NAME DESC");

        }


        $queryInfo->setFetchMode(PDO::FETCH_ASSOC);


        return $queryInfo->fetchAll();
    }




    /**
      * @return table for issue name, number, total views, and date made public (for /issue)
      * Admins can also see issue info that isn't public
    */
    public function getIssues() {

        $User = new User();
        $db = new MyDB();

        $info = $db->catchMistakes("SELECT NUM, IFNULL(NAME, 'N/A') AS NAME, IFNULL(SUM(VIEWS), 0) AS VIEWS, MADEPUB FROM ISSUES
                                    LEFT JOIN PAGEINFO
                                    ON NUM = ISSUE
                                    WHERE (ISPUBLIC = 1 OR ?)
                                    GROUP BY NUM
                                    ORDER BY NUM DESC", $User->isLoggedIn());

        $info->setFetchMode(PDO::FETCH_ASSOC);

        return $info->fetchAll();
    }

}










?>