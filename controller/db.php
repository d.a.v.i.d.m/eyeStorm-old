<?php



require_once("config.php");


class MyDB {


    /**
    * <p>
    * All database related stuff passes through here
    * </p>
    *
    * @param $cmd - query, anything that would be valid sql (SELECT USERS WHERE ID = ?)
    *
    * @param $params - parameters for $query (would give value of question marks)
    *
    * @return the executed query
    */
    public function catchMistakes($cmd, $params = []) {

       // allows for $db->(a query, param) instead of just $db->(a query, [param])
       if (!is_array($params)) {
           $params = [$params];
       }

        try {

         //   $DBH = new PDO("sqlite:" . DB);
           $DBH = new PDO("mysql:host=" . DB_HOST .";dbname=" . DB_NAME ."", DB_USER, DB_PASS);

            $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            $DBH->beginTransaction();


            $query = $DBH->prepare($cmd);


            $query->execute($params);

            $DBH->commit();

            return $query;
        }
        catch(Exception $e) {

            $DBH->rollback();

            $error = $e->getMessage();

            $User = new User();

            if (isset($User->getJWT()->automatedTest)) {
                echo $error . "\n->" . $cmd . "\n"; print_r($params);

                debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                exit;
            }

            $Redirect = new Redirect();

            if (preg_match("/unique/i", $error)) {
                $Redirect->setMessage("error", "unique");
            }
            else if (preg_match("/check/i", $error)) {
                $Redirect->setMessage("error", "check");
            }
            else {

                $Redirect->setMessage("error", "fail");
            }

            $Redirect->toSamePage();
            exit;
        }

    }
}



/*
$User = new User();

$User->create("Deleted", "User Deleted", "a97A65", "a97A65", "meiselesd2018@tabc.org", "3");
*/
/******************************************************************************************
Beyond this point is nothing relevant to day-to-day use. It contains the database structure
*******************************************************************************************/

/********************************************************
For MySQL
*********************************************************/
/*
$db = new MyDB();

$db->catchMistakes("CREATE TABLE IF NOT EXISTS USERS
            (
              ID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
              USERNAME CHAR(20) UNIQUE NOT NULL,
              F_NAME CHAR(10) NOT NULL,
              M_NAME CHAR(3) DEFAULT 'N/A',
              L_NAME CHAR(20) NOT NULL,
      	      PASSWORD TEXT NOT NULL,
              EMAIL VARCHAR(255),
              LEVEL INT(1) NOT NULL CHECK(LEVEL > 0 AND LEVEL < 5),
              AUTH TEXT,
              TWO_FA_ENABLED BOOLEAN DEFAULT 0,
              AUTH_TIME TIMESTAMP(0),
              NOTIFICATIONS BOOLEAN DEFAULT 1,
              UNIQUE (F_NAME, M_NAME, L_NAME)
            );
            ");

$db->catchMistakes("CREATE TABLE IF NOT EXISTS ISSUES
       (
           NUM INTEGER PRIMARY KEY AUTO_INCREMENT,
           ISPUBLIC INTEGER NOT NULL DEFAULT 0,
           NAME VARCHAR(20) UNIQUE,
           MADEPUB DATE
       );");


$db->catchMistakes("CREATE TABLE IF NOT EXISTS PAGEINFO
            (
              ID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
              CREATED DATE,
              URL VARCHAR(75) UNIQUE NOT NULL,
              LEDE BLOB NOT NULL,
      	      IMG_URL BLOB,
              BODY BLOB NOT NULL,
              ISSUE INTEGER NOT NULL,
              AUTHORID VARCHAR(8),
              DISPLAY_ORDER INT(2) DEFAULT 0,
              VIEWS INTEGER NOT NULL DEFAULT 0,
              FOREIGN KEY (ISSUE) REFERENCES ISSUES(NUM),
              UNIQUE (ISSUE, URL)
            );");

$db->catchMistakes("CREATE TABLE IF NOT EXISTS COMMENTS
            (
              ID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
              ART_ID INTEGER NOT NULL,
              AUTHORID INTEGER NOT NULL,
              CONTENT VARCHAR(500),
              CREATED TIMESTAMP(0),
              FOREIGN KEY (ART_ID) REFERENCES PAGEINFO(ID),
              FOREIGN KEY (AUTHORID) REFERENCES USERS(ID)
            );
            ");



$db->catchMistakes("CREATE TABLE IF NOT EXISTS TAGS
            (
              ID INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
              ART_ID INTEGER NOT NULL,
              TAG1 VARCHAR(15) NOT NULL CHECK(TAG1 != TAG2 AND TAG1 != TAG2),
              TAG2 VARCHAR(15) CHECK(TAG2 != TAG1 AND TAG2 != TAG3),
              TAG3 VARCHAR(15) CHECK(TAG3 != TAG1 AND TAG3 != TAG2),
              FOREIGN KEY (ART_ID) REFERENCES PAGEINFO(ID)
            );");
*/



/*
/*$db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1)
SELECT PAGEINFO.ID, SUBSTRING(TAGS, 3, CHAR_LENGTH(TAGS) - 4)
FROM PAGEINFO");*/






?>
