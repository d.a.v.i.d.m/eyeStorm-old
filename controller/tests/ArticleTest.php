<?php


use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");



class ArticleTest extends TestCase {

    public static function setUpBeforeClass() {

        HelpTests::setupForTests();

    }

    public function setup() {

        $Article = new Article();
        $db = new MyDB();


        $maxIssue = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = ?", 0)->fetchColumn();

        // article is created in $this->testCreate, and then the same article is used in a bunch of tests, some of which
        // rename it. It gets deleted at the end of all the tests
        $url = $db->catchMistakes("SELECT URL FROM PAGEINFO WHERE URL IN (?, ?, ?)", ["STORY", "firstStory", rawurlencode("two words")])->fetchColumn();

        $Article->defineInfoFor($maxIssue, $url);

        return $Article;
    }


    // THIS MUST RUN BEFORE ANY AFTER IT
    public function testCreate() {

        $Article = new Article();

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $url = "STORY";

        $createdResult = $Article->create($url, $this->generateArticle(), ["news", "opinion"]);

        $this->assertEquals("/issue/".HelpTests::$privIssue."/story/{$url}", $createdResult["url"]);
    }

    public function testExistsPublic() {

        HelpTests::endSession();
        $db = new MyDB();
        $Article = new Article();

        $url = $db->catchMistakes("SELECT URL FROM PAGEINFO WHERE ID = ?", HelpTests::$pubId)->fetchColumn();

        $this->assertTrue($Article->exists(HelpTests::$pubIssue, $url));
    }

    public function testExistsNotLoggedInPrivateArticle() {

        HelpTests::endSession();
        $db = new MyDB();
        $Article = new Article();

        $url = $db->catchMistakes("SELECT URL FROM PAGEINFO WHERE ID = ?", HelpTests::$privId)->fetchColumn();

        $this->assertFalse($Article->exists(HelpTests::$privIssue, $url));
    }

    public function testGetURL() {

        $Article = $this->setup();

        $this->assertEquals($Article->getURL(), "STORY");
    }

    public function testSetURL() {

        $Article = $this->setup();

        $this->assertTrue($Article->setURL("firstStory"));

        $this->assertEquals($Article->getURL(), "firstStory");
    }

    public function testSetURLWithSpaces() {

        $Article = $this->setup();

        $this->assertTrue($Article->setURL("two words"));

        $this->assertEquals("two%20words", $Article->getURL());
    }

    public function testInvalidEmptyURL() {

        $Article = $this->setup();

        $this->assertFalse($Article->setURL(""));

        $this->assertEquals($Article->getURL(), "two%20words");
    }

    public function testInvalidNullURL() {

        $Article = $this->setup();

        $this->expectException(TypeError::class);
        $this->assertFalse($Article->setURL(null));

        $this->assertEquals($Article->getURL(), "two%20words");
    }

    public function testGetIssue() {

        $Article = $this->setup();
        $db = new MyDB();

        $maxIssue = $db->catchMistakes("SELECT MAX(NUM) FROM ISSUES WHERE ISPUBLIC = ?", 0)->fetchColumn();

        $this->assertEquals($maxIssue, $Article->getIssue());
    }

    public function testGetTags() {

        $Article = $this->setup();
        $this->assertEquals(["news", "opinion"], $Article->getTags());
    }

    public function testNonArrayTag() {

        $Article = $this->setup();

        $this->expectException(TypeError::class);
        $this->assertTrue($Article->setTags("string"));
    }

    public function testSetNoTags() {

        $Article = $this->setup();

        $this->assertFalse($Article->setTags([]));
    }

    public function testSetMultiDimensionTags() {

        $Article = $this->setup();

        $this->assertFalse($Article->setTags(["news", ["other"]]));
    }

    public function testSetTags() {

        $Article = $this->setup();

        $this->assertTrue($Article->setTags(["news", "other"]));

        $this->assertEquals($Article->getTags(), ["news", "other"]);
    }

    public function testSetValidTags() {

        $Article = $this->setup();

        $this->assertEquals(["news", "other"], $Article->getTags());
    }

    public function testListTags() {

        $Article = $this->setup();

        $this->assertEquals("news, other", $Article->listTags());
    }

    public function testGetPublic() {

        $Article = $this->setup();
        $this->assertEquals(0, $Article->getPublic());
    }

    public function testAuthorLevel3CanEdit() {

        $Article = $this->setup();

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $this->assertTrue($Article->canEdit());
    }

    public function testLevel1AuthorCanEdit() {

        $Article = $this->setup();

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");
        $this->assertTrue($Article->canEdit()); // author
    }

    public function testLevel3NonAuthorCanEdit() {

        $Article = $this->setup();

        HelpTests::changeSessions(rand(), 3, bin2hex(random_bytes(6)) . rand(1000, 9999) . "@tabc.org");

        $this->assertTrue($Article->canEdit());
    }

    public function testInvalidLevel2NonAuthorCanEdit() {

        $Article = $this->setup();

        HelpTests::changeSessions(rand(), 2, bin2hex(random_bytes(6)) . rand(1000, 9999) . "@tabc.org");

        $this->assertFalse($Article->canEdit());
    }

    public function testUnauthorizedSave() {

        $Article = $this->setup();

        HelpTests::changeSessions(rand(), 2, bin2hex(random_bytes(6)) . rand(1000, 9999) . "@tabc.org");
        $this->assertFalse($Article->__destruct());
    }

    public function testValidNewViews() {

        $db = new MyDB();
        $Article = new Article();
        HelpTests::endSession();

        [$pubIssue, $url, $initViews] = $this->getArtWherePubStatusIs(1);

        $Article->defineInfoFor($pubIssue, $url);

        $this->assertEquals($initViews, $Article->getViews());

        $this->assertTrue(is_string($Article->getBody()));

        $this->assertEquals($initViews + 1, $db->catchMistakes("SELECT VIEWS FROM PAGEINFO WHERE URL = ? AND ISSUE = ?", [$url, $pubIssue])->fetchColumn());

        $db->catchMistakes("UPDATE PAGEINFO SET VIEWS = VIEWS - 1 WHERE URL = ? AND ISSUE = ?", [$url, $pubIssue]);
    }

    public function testInvalidNewViewsNotPublic() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, HelpTests::$adminEmail);

        $Article = new Article();
        $db = new MyDB();

        [$privIssue, $url, $initViews] = $this->getArtWherePubStatusIs(0);

        $Article->defineInfoFor($privIssue, $url);

        $this->assertEquals($initViews, $Article->getViews());

        $this->assertTrue(is_string($Article->getBody()));

        $this->assertEquals($initViews, $db->catchMistakes("SELECT VIEWS FROM PAGEINFO WHERE URL = ? AND ISSUE = ?", [$url, $privIssue])->fetchColumn());

    }

    public function testInvalidGetBodyPrivArtNotLoggedIn() {

        $Article = new Article();

        HelpTests::endSession();

        [$privIssue, $url, $initViews] = $this->getArtWherePubStatusIs(0);

        $Article->defineInfoFor($privIssue, $url);

        $this->assertFalse($Article->getBody());
    }

    public static function tearDownAfterClass() {

        $ArticleTest = new ArticleTest();
        $Article = $ArticleTest->setup();
        $Article->destroy();

        HelpTests::returnToNormal();
    }

   private function getArtWherePubStatusIs($pubStatus = 1) {

        $db = new MyDB();

        $issue = ($pubStatus) ? HelpTests::$pubIssue : HelpTests::$privIssue;

        $artInfo = $db->catchMistakes("SELECT URL, IFNULL(VIEWS, 0) AS VIEWS FROM PAGEINFO WHERE ISSUE = ? LIMIT 1", $issue);
        $artInfo->setFetchMode(PDO::FETCH_ASSOC);

        $vals = $artInfo->fetch();
        [$url, $initViews] = [$vals["URL"], $vals["VIEWS"]];


        return [$issue, $url, $initViews];
   }



   private function generateArticle() {


        return '<h1>Opinion: Why Does College Matter So Much for Professional Advancement and Financial Security?</h1>
        <h4>January 11, 2017|Ezra Friedman, Ezra Finkelstein, and Aharon Nissel</h4>'

    .'<h5><img alt="" src="https://static.wixstatic.com/media/1af70b5973771d67736218fc1f7b24c8.jpg/v1/fill/w_495,h_358,al_c,q_80,usm_0.66_1.00_0.01/1af70b5973771d67736218fc1f7b24c8.jpg"></h5>'

    .'<section class=\'storyContainer\'><p>With a significant pay gap between college graduates and non-college graduates, no one can deny the importance of college, as it is undoubtedly the most traditional and safest route to a well-paying job. Unfortunately, however, this safe route will take a minimum of three to four years of your life and a large chunk of money, which is likely to leave you paying off debt for years to come. Luckily, in this modern age of the internet, there are thousands of different and cheaper mediums to attain the knowledge necessary to function at high level jobs. Sadly, however, in our society where credentials are valued more than actual knowledge, this is not an option if you truly want to compete with others in certain job markets. College can be a great experience for many people, but due to an abundance offactors, the system in place today often alienates those for whom college is not an ideal option.</p>'

    .'<p>While college is the clear and obvious choice for some people, for other people, it may not be the ideal choice. Some people may not be cut out for college, not specifically because of intelligence, rather, because they are simply in situations where finances take precedent over education. Imagine the scenario where a child from an inner-city went to underfunded, under-staffed, and overall just poor public schools throughout his life. By the time he is in 11th grade he may have poorer math skills than the average American student, and unless Joe Clark (from the film, Lean on Me with Morgan Freeman) himself comes to teach him, there will be little to no chance that college will be an option for this child. And, without achieving a BA, this child will ultimately not be offered the same professional opportunities as others born into more fortunate circumstances. Alas, the bigger question lingers: Why should a child who was simply unlucky enough to be born in an unfortunate neighborhood be relegated to low paying minimum wage jobs for the rest of his life? There have to be more accessible alternatives professional opportunities. where everyone, regardless of background, has an equal chance of advancing in society.</p>'


    .'<p>Aside from the extreme, but all too common, scenario discussed, there are other scenarios for different people, where college may not be the ideal option. The entire formal education system does not work for everyone, and the idea of spending another four years in school taking tests can be a very daunting idea to some high school seniors. As a society we have to move away from the idea that college is the only real option if you want any sort of financial security. Because of the internet, we are blessed with so many different outlets to attain the same knowledge that students learn in college. In order to equalize the playing field for everyone, we should accept that there are many different routes, other than college, that people can take and still end up in the same, financially secure, destination.</p></section>';
   }

}


?>