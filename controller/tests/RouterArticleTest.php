<?php

use PHPUnit\Framework\TestCase;

require_once("../vendor/autoload.php");


class RouterUserTest extends TestCase {

    public static function setUpBeforeClass() {
        HelpTests::setupForTests();
    }

    /**************Story****************/

    /***GET***/

    /**
      * Gets article through api
      *
      * @param $id - id of article to request
      * @param $loggedIn - boolean if user should be loggedin or not
      * @param $lvl - lvl of user requesting api
      * @param $actualInfo - ["ISSUE"] = issue, ["URL"] = url of article. For testing nonexistant articles
      *
      * @return api response
      */
    private function getArticle($id, $loggedIn = true, $lvl = 3, array $actualInfo = ["ISSUE" => null, "URL" => null]) {


            $db = new MyDB();

            $newUser = HelpTests::createNewUser($lvl);

            HelpTests::helpSetSessions($newUser, $lvl, $loggedIn);

            $artInfoQuery = $db->catchMistakes("SELECT ISSUE, URL FROM PAGEINFO WHERE ID = ?", $id);
            $artInfoQuery->setFetchMode(PDO::FETCH_ASSOC);
            $artInfo = $artInfoQuery->fetchAll();

            $artInfo = (!empty($artInfo[0]["ISSUE"])) ? $artInfo[0] : $actualInfo;


            $res = HelpTests::createHTTPRequest("story", ["issue" => $artInfo["ISSUE"],
                                                         "name" => $artInfo["URL"]],
                                            "GET");

            HelpTests::deleteNewUser($newUser["username"]);
            return $res;

    }

    public function testGetNonexistantURLExistantIssue() {

        foreach([HelpTests::$pubIssue, HelpTests::$privIssue] as $issue) {

            $result = $this->getArticle("", true, 3, ["ISSUE" => $issue,
                                                                    "URL" => "test".bin2hex(random_bytes(10))]);

            $this->assertEquals(404, $result);
        }
    }

    public function testGetNonexistantIssueExistantURL() {

        $db = new MyDB();

        foreach ([HelpTests::$pubId, HelpTests::$privId] as $artId) {

            $url = $db->catchMistakes("SELECT URL FROM PAGEINFO WHERE ID = ?", $artId)->fetchColumn();
            $result = $this->getArticle("", true, 3, ["ISSUE" => HelpTests::$privIssue + 1, "URL" => $url]);

            $this->assertEquals(404, $result);
        }

    }

    public function testBadGetPrivateArticleNotLoggedIn() {

        $result = $this->getArticle(HelpTests::$privId, false);
        $this->assertEquals(404, $result);
    }

    public function testGoodGetPublicArticleNotLoggedIn() {

        $article = $this->getArticle(HelpTests::$pubId, false);
        $expectedKeys = ["BODY", "TAGS", "COMMENTS", "ID", "CAN_EDIT"];

        $this->assertEquals($expectedKeys, array_keys((array)$article));
    }

    public function testGetPublicArticleYesLoggedInDifferentLevels() {

        foreach([1, 2, 3] as $lvl) {

            $article = $this->getArticle(HelpTests::$pubId, true, $lvl);
            $expectedKeys = ["BODY", "TAGS", "COMMENTS", "ID", "CAN_EDIT"];

            $this->assertEquals($expectedKeys, array_keys((array)$article));
        }
    }

    public function testGetPrivateArticleYesLoggedInDifferentLevels() {

        foreach([1, 2, 3] as $lvl) {

            $article = $this->getArticle(HelpTests::$privId, true, $lvl);
            $expectedKeys = ["BODY", "TAGS", "COMMENTS", "ID", "CAN_EDIT"];

            $this->assertEquals($expectedKeys, array_keys((array)$article));
        }
    }

    public function testGetArticleWithSpacesInURL() {

        $url = "test%20twoWords";

        $db = new MyDB();

        $db->catchMistakes("INSERT INTO PAGEINFO (URL, LEDE, BODY, AUTHORID, ISSUE, IMG_URL) VALUES (?, ?, ?, ?, ?, ?)",
            [$url, HelpTests::$lede, HelpTests::$body, HelpTests::$adminId, HelpTests::$privIssue, json_encode([])]);

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE URL = ? AND ISSUE = ?", [$url, HelpTests::$privIssue])->fetchColumn();

        $db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1) VALUES(?, ?)", [$artId, HelpTests::$tags[0]]);

        $result = $this->getArticle($artId, true, 3);

        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID = ?", $artId);
        $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID = ?", $artId);

        $expectedKeys = ["BODY", "TAGS", "COMMENTS", "ID", "CAN_EDIT"];
        $this->assertEquals($expectedKeys, array_keys((array)$result));
    }

    /***PUT***/

    /**
      * Helper function to update articles. Creates new article then updates it
      *
      * @param $public - boolean if should edit public or private article
      * @param $lvl - level of user trying to edit
      * @param $author - boolean
      * @param $loggedIn - boolean
      */
    private function createArticle($public, $lvl, $author, $loggedIn = true, $pics = []) {

        $db = new MyDB();

        $authorUser = HelpTests::createNewUser(1, "testAuthor");
        $otherUser = HelpTests::createNewUser($lvl, "testOther");

        $issueType = ($public) ? HelpTests::$pubIssue : HelpTests::$privIssue;
        $url = "testPut";
        $lede = "beforeEditLede";
        $body = "beforeEditBody";
        $resultPics = [];
        $numDataPics = -1;

        // if not author, login as random user, else login as author
        HelpTests::helpSetSessions($otherUser, $lvl, $loggedIn && !$author);

        if ($loggedIn && $author) {

            HelpTests::helpSetSessions($authorUser, $lvl, $loggedIn && $author);
        }

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [$issueType, $url, $lede, $body, json_encode($pics),
                           $authorUser["id"], json_encode([1])]);

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? AND URL = ?", [$issueType, $url])->fetchColumn();
        $db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1) VALUES(?, ?)", [$artId, "reaction"]);

        $afterEditLede = "<h1>Title</h1><h4>author</h4><p>Hello</p>";
        $afterEditBody = "<p>body</p>";

        for($i = 0; $i < count($pics); $i++) {

            $afterEditBody = '<img src="'.$pics[$i].'" alt="" />' . $afterEditBody;

            if (strpos($pics[$i], "data:image") === false) {
                $resultPics[] = $pics[$i];
            }
            else {
                $resultPics[] = "/images/issue/{$issueType}/{$artId}/{$i}.png";
                $numDataPics++;
            }
        }


        $resultPics = json_encode(array_reverse($resultPics));



        $res = HelpTests::createHTTPRequest("story", ["issue"=>$issueType, "name"=>$url, "edit" => $afterEditLede . $afterEditBody], "PUT");

        $afterEditBody = preg_replace("/src=\"[\s\S]+?\"/", "data-src", $afterEditBody);

        $query = $db->catchMistakes("SELECT LEDE, BODY, IMG_URL FROM PAGEINFO WHERE ID = ?", $artId);
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $editedVersion = ($result = $query->fetchAll()) ? $result[0] : [];


        while($numDataPics >= 0) {

            if (!file_exists(__DIR__."/../../public/images/issue/{$issueType}/{$artId}/{$numDataPics}.png")) {
                return false;
            }
            $numDataPics--;
        }

        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID = ?", $artId);
        $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID = ?", $artId);
        HelpTests::deleteNewUser($authorUser["username"]);
        HelpTests::deleteNewUser($otherUser["username"]);

        $this->deletePics($issueType, $artId);

        return ["LEDE"=>$afterEditLede, "BODY"=>$afterEditBody, "IMG_URL"=>$resultPics] == $editedVersion && !file_exists(__DIR__."/../../public/images/issue/{$issueType}/{$artId}/");
    }

    private function deletePics($issue, $artId) {

        if (file_exists(__DIR__."/../../public/images/issue/{$issue}/{$artId}")) {
            array_map('unlink', glob(__DIR__."/../../public/images/issue/{$issue}/{$artId}/*.*"));
            rmdir(__DIR__."/../../public/images/issue/{$issue}/{$artId}");
        }

    }

    public function testBadUpdatePrivateArticleNotLoggedIn() {

        $result = $this->createArticle(false, 1, false, false);
        $this->assertFalse($result);
    }

    public function testBadUpdatePublicArticleNotLoggedIn() {

        $result = $this->createArticle(true, 1, false, false);
        $this->assertFalse($result);
    }

    public function testBadUpdatePrivateArticleNotAuthorNotLvlThree() {

        foreach([1, 2] as $lvl) {

            $result = $this->createArticle(false, $lvl, false, true);
            $this->assertFalse($result);
        }
    }

    public function testBadUpdatePublicArticleNotAuthorNotLvlThree() {

        foreach([1, 2] as $lvl) {

            $result = $this->createArticle(true, $lvl, false, true);
            $this->assertFalse($result);
        }
    }

    public function testGoodUpdatePrivateArticleLevelThree() {

        $result = $this->createArticle(false, 3, false);
        $this->assertTrue($result);
    }

    public function testGoodUpdatePublicArticleLevelThree() {

        $result = $this->createArticle(true, 3, false, true);
        $this->assertTrue($result);
    }

    public function testBadUpdatePublicArticleAuthor() {

        foreach([1, 2, 3] as $lvl) {

            $result = $this->createArticle(false, $lvl, true, true);
            $this->assertTrue($result);
        }
    }

    // checks if data:// uris can be put in
    public function testGoodUpdateDataPics() {

        $result = $this->createArticle(true, 3, true, true, [HelpTests::$dataURI]);
        $this->assertTrue($result);
    }

    public function testGoodUpdateDataAndRegularPics() {

        $result = $this->createArticle(true, 3, true, true, [HelpTests::$dataURI, HelpTests::$pics[0]]);
        $this->assertTrue($result);
    }

    public function testBadUpdatePublicArticleAuthorNotLevelThree() {

        foreach([1, 2] as $lvl) {

            $result = $this->createArticle(true, $lvl, true);
            $this->assertFalse($result);
        }
    }


    /***POST***/

    private function helpPOSTArticles($article, $tags = ["reaction"], $url = "testValid", $loggedIn = true, $lvl = 1) {

        $author = HelpTests::createNewUser($lvl, "author");
        $db = new MyDB();

        HelpTests::helpSetSessions($author, $lvl, $loggedIn);

        HelpTests::createHTTPRequest("story", ["name" => $url, "txtArea"=>$article, "type[]" => $tags], "POST");

        $artInfoQuery = $db->catchMistakes("SELECT URL, LEDE, BODY, AUTHORID, TAG1, TAG2, TAG3, PAGEINFO.ID AS ID
                                              FROM PAGEINFO
                                              LEFT JOIN TAGS ON ART_ID = PAGEINFO.ID
                                              WHERE ISSUE = ? AND URL = ?", [HelpTests::$privIssue, rawurlencode($url)]);
        $artInfoQuery->setFetchMode(PDO::FETCH_ASSOC);
        $artInfo = ($result = $artInfoQuery->fetchAll()) ? $result[0] : ["ID"=>null];



        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID = ?", $artInfo["ID"]);
        $artInfoQuery = $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID = ?", $artInfo["ID"]);
        $this->deletePics(HelpTests::$privIssue, $artInfo["ID"]); // if data:// pics
        HelpTests::deleteNewUser($author["username"]);

        if (count($artInfo) == 1) {// if article wasn't created
            return false;
        }

        $expectedBodyAndLede = preg_replace("/src=\"[^\"]+\"/", "data-src", $article);

        return $expectedBodyAndLede == $artInfo["LEDE"] . $artInfo["BODY"] &&
               array_merge($tags, array_fill(0, 3 - count($tags),null)) == [$artInfo["TAG1"], $artInfo["TAG2"], $artInfo["TAG3"]] &&
               $artInfo["AUTHORID"] == $author["id"];
    }

    public function testBadArticleNotLoggedIn() {

        $result = $this->helpPOSTArticles(HelpTests::$basicArticle, ["reaction"], "testValid", false);

        $this->assertFalse($result);
    }


    public function testBadArticleNoHeading() {

        $result = $this->helpPOSTArticles("<h4>Author</h4><p>Lede</p><p>Body</p>");

        $this->assertFalse($result);
    }

    public function testBadArticleNoAuthor() {

        $result = $this->helpPOSTArticles("<h1>Author</h1><p>Lede</p><p>Body</p>");

        $this->assertFalse($result);
    }

    public function testBadArticleNoBody() {

        $result = $this->helpPOSTArticles("<h1>Author</h1><h4>Author</h4>");

        $this->assertFalse($result);
    }

    // invalid means not 1d array of strings
    public function testBadArticleInvalidTags() {

        foreach([[], [[]], [1], ["string", []], null, true] as $tag) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle, $tag);
            $this->assertFalse($result);
        }
    }

    public function testBadArticleDuplicateTags() {

        foreach([["reaction", "reaction"], ["opinion", "reaction", "opinion"]] as $tag) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle, $tag);
            $this->assertFalse($result);
        }
    }


    // this should be accepted but bad code should be removed, or should just not be accepted
    public function testBadArticleMaliciousCode() {

        $badCode = [
            "<script></script>",
            "<?php ?>",
            "<?= ?>",
            "<link href=''>"
        ];

        foreach ($badCode as $code) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle . $code);
            $this->assertFalse($result);
        }
    }

    public function testBadArticleBadURIs() {

        $badURIs = [
            "",
            null,
            true,
            "<script>"
        ];

        foreach ($badURIs as $uri) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle, ["reaction"], $uri);
            $this->assertFalse($result);
        }
    }


    public function testGoodArticleAllLvls() {

        foreach([1, 2, 3] as $lvl) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle, ["reaction"], "testValid", true, $lvl);
            $this->assertTrue($result);
        }
    }

    // 1, 2, or 3 tags
    public function testGoodArticleDifferentNumOfTags() {

        $tags = [
            ["reaction"],
            ["reaction", "opinion"],
            ["reaction", "opinion", "poll"]
        ];

        foreach($tags as $tag) {

            $result = $this->helpPOSTArticles(HelpTests::$basicArticle, $tag);
            $this->assertTrue($result);
        }
    }

    public function testGoodArticleURLsWithSpaces() {

        $result = $this->helpPOSTArticles(HelpTests::$basicArticle, ["reaction"], "this test");
        $this->assertTrue($result);
    }

    public function testGoodArticleRegularPics() {

        $result = $this->helpPOSTArticles('<h1>Title</h1><h4>Author</h4><p>Hi<img src="'.HelpTests::$pics[0].'" alt="" /></p>');
        $this->assertTrue($result);
    }

    /***DELETE***/

    /**
      * Creates user then does POST to api to delete user with parameters given
      *
      */
    private function helpDeleteArticles($lvl, $author = false, $public = true, $goodPassword = true, $loggedIn = true) {

        $db = new MyDB();

        $authorUser = HelpTests::createNewUser(1, "author");
        $toDelete = HelpTests::createNewUser($lvl, "deleter");

        if ($loggedIn && !$author) {
            HelpTests::changeSessions($toDelete["id"], $lvl, $toDelete["username"]);
        }
        else if ($loggedIn && $author) {

            HelpTests::changeSessions($authorUser["id"], $lvl, $authorUser["username"]);
        }
        else {
            HelpTests::endSession();
        }

        $issue = ($public) ? HelpTests::$pubIssue : HelpTests::$privIssue;
        $password = ($goodPassword) ? HelpTests::$adminPass : "bad password";
        $url = "testDelete";

        $db->catchMistakes("INSERT INTO PAGEINFO (ISSUE, CREATED, URL, LEDE, BODY, IMG_URL,
                           AUTHORID, SLIDE_IMG) VALUES(?, CURDATE(), ?, ?, ?, ?, ?, ?)",
                           [$issue, $url, HelpTests::$basicArticle, "<p>hi</p>", json_encode([]),
                           $authorUser["id"], json_encode([])]);

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? AND URL = ?", [$issue, $url])->fetchColumn();
        $db->catchMistakes("INSERT INTO TAGS (ART_ID, TAG1) VALUES(?, ?)", [$artId, "reaction"]);

        HelpTests::createHTTPRequest("story", ["issue" => $issue, "name" => $url, "password" => $password], "DELETE");

        $deletedCheck = $db->catchMistakes("SELECT URL, TAG1 FROM PAGEINFO, TAGS WHERE ? IN (PAGEINFO.ID, ART_ID)", $artId)->fetchAll();

        // just in case
        $db->catchMistakes("DELETE FROM TAGS WHERE ART_ID = ?", $artId);
        $artInfoQuery = $db->catchMistakes("DELETE FROM PAGEINFO WHERE ID = ?", $artId);
        $this->deletePics($issue, $artId); // if data:// pics
        HelpTests::deleteNewUser($authorUser["username"]);
        HelpTests::deleteNewUser($toDelete["username"]);

        return !$deletedCheck;
    }

    public function testBadDeleteArticleNotLoggedIn() {

        foreach([true, false] as $isPublic) {
            $result = $this->helpDeleteArticles(1, true, $isPublic, true, false);
            $this->assertFalse($result);
        }
    }

    public function testBadDeleteNotAuthorNotLvlThree() {

        foreach([1, 2] as $lvl) {

            $result = $this->helpDeleteArticles($lvl);
            $this->assertFalse($result);
        }
    }

    public function testBadDeleteAuthorBadPassword() {

        $result = $this->helpDeleteArticles(3, true, false, false);
        $this->assertFalse($result);
    }

    public function testBadDeleteLvlThreeBadPassword() {

        $result = $this->helpDeleteArticles(3, false, false, false);
        $this->assertFalse($result);
    }

    public function testGoodDeleteAuthor() {

        foreach([1, 2, 3] as $lvl) {

            $result = $this->helpDeleteArticles($lvl, true, false);
            $this->assertTrue($result);
        }
    }

    public function testGoodDeleteArticleLevelThree() {

        $result = $this->helpDeleteArticles(3);
        $this->assertTrue($result);
    }

    public function testGoodAuthorDeletePrivateAndPublicArticles() {

        foreach ([true, false] as $pubStatus) {

            $result = $this->helpDeleteArticles(1, true, $pubStatus);
            $this->assertTrue($result);
        }
    }

    public function testGoodLvlThreeDeletePrivateAndPublicArticles() {

        foreach ([true, false] as $pubStatus) {

            $result = $this->helpDeleteArticles(3, false, $pubStatus);
            $this->assertTrue($result);
        }
    }





    public static function tearDownAfterClass() {
        HelpTests::returnToNormal();
    }

}




?>