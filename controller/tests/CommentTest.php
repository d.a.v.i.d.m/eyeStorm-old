<?php


use PHPUnit\Framework\TestCase;

session_start();
require_once("../vendor/autoload.php");



class CommentTest extends TestCase {


    public static function setUpBeforeClass() {

        HelpTests::setupForTests();
    }

    public function testBadCreateNotLoggedIn() {

        HelpTests::endSession();

        $Comment = new Comment();

        $this->assertFalse($Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], "This is valid text"));
    }

    public function testBadCreateShort() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();

        $this->assertFalse($Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], ""));
    }

    public function testBadCreateBadURL() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();

        $this->assertFalse($Comment->create(HelpTests::$pubIssue, "test badURL", "This is valid text")); // highly unlikely there will be an id of that
    }

    public function testBadCreateNullURL() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? LIMIT 1", HelpTests::$pubIssue)->fetchColumn();

        $this->expectException(TypeError::class);
        $this->assertFalse($Comment->create(HelpTests::$pubIssue, null, "This is a valid comment"));
    }

    public function testBadCreateNullText() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $artId = $db->catchMistakes("SELECT ID FROM PAGEINFO WHERE ISSUE = ? LIMIT 1", HelpTests::$pubIssue)->fetchColumn();

        $this->expectException(TypeError::class);
        $this->assertFalse($Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], null));
    }

    public function testGoodCreate() {

         HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();

        $this->assertInternalType("int", $Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], "This is valid text"));
    }

    public function testGoodCreateHTMLTags() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $content = "<p>Hi</p> <em>This</em><strong>Is</strong><abbr>Text</abbr>";

        $commentId = $Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], $content);

        $dbContent = $db->catchMistakes("SELECT CONTENT FROM COMMENTS WHERE ID = ?", $commentId)->fetchColumn();

        $this->assertEquals($content, $dbContent);
    }

    public function testGoodStripScriptTag() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $content = "<p>Hi</p> <em>This</em><strong>Is</strong><abbr>Text</abbr>";

        $commentId = $Comment->create(HelpTests::$pubIssue, HelpTests::$url[0], "<script></script>".$content);

        $dbContent = $db->catchMistakes("SELECT CONTENT FROM COMMENTS WHERE ID = ?", $commentId)->fetchColumn();

        $this->assertEquals($content, $dbContent);
    }

    public function testGetContent() {

        HelpTests::endSession();

        $Comment = new Comment();
        $db = new MyDB();

        $commentQuery = $db->catchMistakes("SELECT ID, CONTENT FROM COMMENTS LIMIT 1");
        $commentQuery->setFetchMode(PDO::FETCH_NUM);

        list($commentId, $content) = $commentQuery->fetchAll()[0];

        $Comment->defineInfo($commentId);

        $this->assertEquals($content, $Comment->getContent());
    }

    public function testGetCreated() {

        $db = new MyDB();
        $Comment = new Comment();

        $commentQuery = $db->catchMistakes("SELECT ID, CREATED FROM COMMENTS LIMIT 1");
        $commentQuery->setFetchMode(PDO::FETCH_NUM);

        list($id, $created) = $commentQuery->fetchAll()[0];

        $Comment->defineInfo($id);

        $this->assertEquals($created, $Comment->getCreated());
    }

    public function testGetAuthorId() {

        $db = new MyDB();
        $Comment = new Comment();

        $commentQuery = $db->catchMistakes("SELECT ID, AUTHORID FROM COMMENTS LIMIT 1");
        $commentQuery->setFetchMode(PDO::FETCH_NUM);

        list($id, $authorId) = $commentQuery->fetchAll()[0];

        $Comment->defineInfo($id);

        $this->assertEquals($authorId, $Comment->getAuthorId());
    }

    public function testArticleId() {

        $db = new MyDB();
        $Comment = new Comment();

        $commentQuery = $db->catchMistakes("SELECT ID, ART_ID FROM COMMENTS LIMIT 1");
        $commentQuery->setFetchMode(PDO::FETCH_NUM);

        list($id, $artId) = $commentQuery->fetchAll()[0];

        $Comment->defineInfo($id);

        $this->assertEquals($artId, $Comment->getArticleId());
    }

    public function testBadDeleteNotLoggedIn() {

        HelpTests::endSession();

        $Comment = new Comment();
        $db = new MyDB();

        $commentId = $db->catchMistakes("SELECT ID FROM COMMENTS WHERE AUTHORID = ? LIMIT 1", HelpTests::$adminId)->fetchColumn();

        $Comment->defineInfo($commentId);

        $this->assertFalse($Comment->delete());
    }

    public function testBadDeleteLoggedInNotAuthor() {

        HelpTests::changeSessions(HelpTests::$adminId, 1, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $commentId = $db->catchMistakes("SELECT ID FROM COMMENTS WHERE AUTHORID = ? LIMIT 1", HelpTests::$test1Id)->fetchColumn();

        $Comment->defineInfo($commentId);
        $this->assertFalse($Comment->delete());
    }

    public function testGoodDeleteAuthor() {

        HelpTests::changeSessions(HelpTests::$test1Id, 1, "test1");

        $Comment = new Comment();
        $db = new MyDB();

        $commentId = $db->catchMistakes("SELECT ID FROM COMMENTS WHERE AUTHORID = ? LIMIT 1", HelpTests::$test1Id)->fetchColumn();

        $Comment->defineInfo($commentId);

        $this->assertTrue($Comment->delete());

        $deletedUserId = $db->catchMistakes("SELECT ID FROM USERS WHERE USERNAME = ?", "Deleted")->fetchColumn();
        $deletedCommentQuery = $db->catchMistakes("SELECT AUTHORID, CONTENT FROM COMMENTS WHERE ID = ?", $commentId);
        $deletedCommentQuery->setFetchMode(PDO::FETCH_NUM);

        list($deletedCommentAuthorId, $deletedContents) = $deletedCommentQuery->fetchAll()[0];

        $this->assertEquals($deletedUserId, $deletedCommentAuthorId);
        $this->assertEquals("deleted", $deletedContents);
    }

    public function testGoodDeleteAdmin() {

        HelpTests::changeSessions(HelpTests::$adminId, 3, "admin");

        $Comment = new Comment();
        $db = new MyDB();

        $commentId = $db->catchMistakes("SELECT ID FROM COMMENTS WHERE AUTHORID = ? LIMIT 1", HelpTests::$test1Id)->fetchColumn();

        $Comment->defineInfo($commentId);
        $this->assertTrue($Comment->delete());
    }


    public static function tearDownAfterClass() {

        HelpTests::returnToNormal();
    }

}


?>