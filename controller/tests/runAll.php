<?php

$toTest = [
    '../classes/Article.php' => './ArticleTest.php',
    '../classes/ArticleGroup.php' => './ArticleGroupTest.php',
    '../classes/Info.php' => './InfoTest.php',
    '../classes/Issue.php' => './IssueTest.php',
    '../classes/sendMail.php' => './SendMailTest.php',
    '../classes/UserGroup.php' => './UserGroupTest.php',
    '../classes/User.php' => './UserTest.php',
    '../classes/Comment.php' => './CommentTest.php',
    '../classes/Router.php' => './RouterArticleGroupTest.php',
    '../classes/Router.php' => './RouterArticleTest.php',
    '../classes/Router.php' => './RouterCommentTest.php',
    '../classes/Router.php' => './RouterIssueTest.php',
    '../classes/Router.php' => './RouterUserGroupTest.php',
    '../classes/Router.php' => './RouterUserTest.php'
];

foreach ($toTest as $class => $test) {

    echo `~/.composer/vendor/phpunit/phpunit/phpunit -v --bootstrap {$class} {$test} --stderr`;
}

?>
